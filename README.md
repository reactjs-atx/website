# ReactJS ATX 

```bash
$ npm install
$ npm start
```

## Summary

Webpack brings all the technologies together!

### Resources

* https://github.com/webpack/webpack
* https://github.com/webpack/webpack-cli
* https://github.com/webpack/webpack-dev-server
* https://github.com/jantimon/html-webpack-plugin
* https://github.com/TypeStrong/ts-node
* https://github.com/TypeStrong/fork-ts-checker-webpack-plugin
* https://github.com/webpack-contrib/eslint-webpack-plugin
* https://github.com/babel/babel-loader
